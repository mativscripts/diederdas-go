package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"time"
)

type Word struct {
	Word    string `json:"word"`
	Article string `json:"article"`
}

type Words struct {
	Version string `json:"version"`
	Data    []Word `json:"data"`
}

func main() {
	file, _ := os.Open("words.json")
	defer file.Close()

	decoder := json.NewDecoder(file)
	words := Words{}
	err := decoder.Decode(&words)
	if err != nil {
		fmt.Println("Error:", err)
	}

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(words.Data), func(i, j int) { words.Data[i], words.Data[j] = words.Data[j], words.Data[i] })

	reader := bufio.NewReader(os.Stdin)
	score := 0
	for i := 0; i < 10; i++ {
		fmt.Printf("What is the article for %s?\n1. die\n2. der\n3. das\n", words.Data[i].Word)
		text, _ := reader.ReadString('\n')

		if (text[0] == '1' && words.Data[i].Article == "die") ||
			(text[0] == '2' && words.Data[i].Article == "der") ||
			(text[0] == '3' && words.Data[i].Article == "das") {
			score++
		}
	}
	fmt.Printf("Your score is %d/10\n", score)
	fmt.Print("Press 'Enter' to replay or 'q' to quit\n")
	text, _ := reader.ReadString('\n')
	if text[0] != 'q' {
		main()
	}
}
