# Go CLI German Article Quiz

CLI Go program that quizzes you on German articles for words. The program presents you with a set of questions and evaluates your answers to provide a final score.
JSON database from by fodpeter here:
https://github.com/fodpeter/der-die-das/blob/master/public/words.json

## Get Started

1. Make sure you have [Go](https://golang.org/dl/) installed on your computer.

2. Clone or download this repository.

3. Open a terminal and navigate to the directory where `diederdas-go.go` file is located.

4. Compile the program using the command:

   ```bash
   go build diederdas-go.go

5. Run it:

   ```bash
   ./diederdas-go


## License
GNU GPLv3